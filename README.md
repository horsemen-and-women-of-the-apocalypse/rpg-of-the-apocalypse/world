# World service

<p align="center">A REST API exposing endpoints for world management</p>

## Requirements

* NodeJS (version: `16.13.0`)

### MongoDB database
* Instance of MongoDB (Version: `4.4`)

## API documentation

The documentation of routes is defined by `http://{url}/docs/`

## Set-up for development

### Install dependencies

```
npm install
```

### Create configuration

Create a configuration `app.development.ini` in `config` based on the template configuration.

### Run

```
npm run start:dev
```

Server will automatically restart when a change in files is detected.

## Common commands

### Tests

Create a configuration `app.test.ini` in `config` based on the template configuration.

```
npm run test
```

### Code style

`npm run lint` to check code style and `npm run lint:fix` to fix it.
