# Build stage
FROM node:16.13.0 as build-stage

# Copy app dependencies
COPY package*.json ./

# Download dependencies
RUN npm install

# Copy app
COPY . .

# Set-up configuration files sym links
RUN mkdir -p ./config
RUN ln -s /run/secrets/app.production.ini ./config/app.production.ini

RUN npm run build

# Pre-production stage
FROM node:16.13.0 AS pre-production-stage

# Grant app folder ownership to node user (as WORKDIR creates a folder owned by root)
RUN mkdir -p /home/node/app && chown -R node:node /home/node/app
WORKDIR /home/node/app

COPY --chown=node:node --from=build-stage node_modules node_modules
COPY --chown=node:node --from=build-stage config config
COPY --chown=node:node . .

RUN ln -s /run/secrets/app.test.ini ./config/app.test.ini

EXPOSE 8082

USER node

CMD [ "npm", "run", "start:prod" ]

# Production stage
FROM node:16.13.0 AS production-stage

# Grant app folder ownership to node user (as WORKDIR creates a folder owned by root)
RUN mkdir -p /home/node/app && chown -R node:node /home/node/app
WORKDIR /home/node/app

COPY --chown=node:node --from=build-stage node_modules node_modules
COPY --chown=node:node --from=build-stage config config
COPY --chown=node:node --from=build-stage dist .
COPY --chown=node:node --from=build-stage package.json ./

EXPOSE 8082

USER node

# Start app
CMD [ "npm", "run", "exec:prod", "--", "node", "main/index.js" ]