/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import cors, { CorsOptions } from "cors";
import express, { NextFunction, Request, Response } from "express";
import { Server } from "http";
import responseTime from "response-time";
import LOGGER from "./logger";
import routes from "./routes";
import config from "./config/service";
import swaggerUi from "swagger-ui-express";
import swaggerConfig from "./config/swagger";
import { ApiError, ApiErrorResponse } from "@rpga/common";
import mongoose from "mongoose";

// Create server
const app = express();
const server = new Server(app);

// Set up CORS
const corsConfig: CorsOptions = { origin: true, credentials: true };
app.use(cors(corsConfig));

// Use JSON decoder for "application/json" body
app.use(express.json());

// Add response time
app.use(responseTime((req, res, time) => {
    // req.baseUrl exits, trust me :)
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    LOGGER.info(`[Express] ${req.method} ${(req as any).baseUrl} in ${time.toFixed(3)}ms => response code: ${res.statusCode}`);
}));

// Add routes
for (const route of routes) {
    app.use(route.base, route.router);
}

// Add Swagger UI
app.use("/docs", swaggerUi.serve, swaggerUi.setup(swaggerConfig));

// Start server
const PORT = config.http.port;
server.listen(PORT, async() => {
    LOGGER.info(`[Main] App listening on port: ${PORT} (mode: ${process.env.NODE_ENV})`);

    try {
        // Set up error handler
        app.use((error: Error, req: Request, res: Response, next: NextFunction) => {
            LOGGER.error(error);

            // Send response
            res.status(error instanceof ApiError ? (error as ApiError).code : 500).json(new ApiErrorResponse(error.message));
            next();
        });

        // Connect to db
        await mongoose.connect(config.database.uri, {
            user: config.database.user,
            pass: config.database.password
        });

        LOGGER.info(`Successfully connected to ${config.database.uri}`);
    } catch (e) {
        LOGGER.error((e as Error).stack);
        process.exit(1);
    }
});

export default server;

