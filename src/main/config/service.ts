/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import fs from "fs";
import LOGGER from "../logger";
import ini from "ini";

type IniSection = { [property: string]: string };
type Ini = { [section: string]: IniSection };

/**
 * HTTP Server configuration
 */
class HttpConfiguration {
    /** Port number */
    public readonly port: number;

    /**
     * Constructor
     *
     * @param conf Configuration as an object
     */
    constructor(conf: IniSection) {
        const port = Number.parseInt(conf.port);
        if (!Number.isInteger(port)) {
            throw new Error(`http.number (value: ${port}) isn't an integer in the configuration file`);
        }
        this.port = port;
    }
}

/**
 * Database Server configuration
 */
class DatabaseConfiguration {
    public readonly uri: string;
    public readonly user: string;
    public readonly password: string;

    /**
     * Constructor
     *
     * @param conf Configuration as an object
     */
    constructor(conf: IniSection) {
        this.uri = conf.uri;
        this.user = conf.user;
        this.password = conf.password;
    }
}

/**
 * Service configuration
 */
class Configuration {
    public readonly http: HttpConfiguration;
    public readonly database: DatabaseConfiguration;

    /**
     * Constructor
     *
     * @param conf Configuration as an object
     */
    constructor(conf: Ini) {
        const http = conf.http,
            database = conf.database;

        if (http === undefined || http === null) {
            throw new Error("http section is undefined in the configuration file");
        }

        if (database === undefined || database === null) {
            throw new Error("http section is undefined in the configuration file");
        }
        this.http = new HttpConfiguration(http);
        this.database = new DatabaseConfiguration(database);
    }
}

// Load configuration
const configPath = "./config/app." + process.env.NODE_ENV + ".ini";
const config = new Configuration(ini.parse(fs.readFileSync(configPath, { encoding: "utf-8" })));
LOGGER.info("Load configuration from: " + configPath);

export default config;
