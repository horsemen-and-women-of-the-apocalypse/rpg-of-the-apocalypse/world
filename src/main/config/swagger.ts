/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included.
 */
import { createApiResponseDefinition } from "@rpga/common";

const swaggerConfig = {
    "swagger": "2.0",
    "info": {
        "version": process.env.npm_package_version,
        "title": "RPG of the Apocalypse - World service",
        "description": "Documentation describing the endpoints of the world service",
        "license": {
            "name": "MIT",
            "url": "https://opensource.org/licenses/MIT"
        }
    },
    "tags": [
        {
            "name": "Version",
            "description": "API for version"
        }
    ],
    "basePath": "/",
    "schemes": [
        "http"
    ],
    "consumes": [
        "application/json"
    ],
    "produces": [
        "application/json"
    ],
    "paths": {
        "/version": {
            "get": {
                "tags": [ "Version" ],
                "summary": "Get the version of the API",
                "responses": {
                    "200": {
                        "description": "The version of the API",
                        "schema": {
                            "$ref": "#/definitions/VersionResponse"
                        }
                    }
                }
            }
        },
        "/maps": {
            "get": {
                "tags": [ "Maps" ],
                "summary": "Return all maps stored",
                "parameters": [
                    {
                        "in": "query",
                        "name": "search",
                        "description": "Search by map name",
                        "required": false,
                        "schema": {
                            "type": "string",
                        }
                    },
                    {
                        "in": "query",
                        "name": "start",
                        "description": "Start limit to return maps",
                        "required": false,
                        "schema": {
                            "type": "integer",
                            "minimum": 0
                        }
                    },
                    {
                        "in": "query",
                        "name": "size",
                        "description": "Max number of maps returned",
                        "required": false,
                        "schema": {
                            "type": "integer",
                            "minimum": 0,
                        }
                    }

                ],
                "responses": {
                    "200": {
                        "schema": {
                            "$ref": "#/definitions/AllMapsGetResponse"
                        }
                    },
                    "400": {
                        "description": "Error occured, more details in body.error",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    }
                }
            },
            "post": {
                "tags": [ "Maps" ],
                "summary": "Add a map to the world",
                "parameters": [
                    {
                        "in": "body",
                        "name": "body",
                        "description": "To access a tile, tileMap[y][x][layer]",
                        "required": true,
                        "schema": {
                            "type": "object",
                            "properties": {
                                "name": { "type": "string" },
                                "tileMap": {
                                    "type": "array",
                                    "items": {
                                        "type": "array",
                                        "items": {
                                            "type": "array",
                                            "minItems": "5",
                                            "maxItems": "5",
                                            "items": { "type": "integer" }
                                        }
                                    }
                                },
                                "hitboxMap": {
                                    "type" : "array",
                                    "items": {
                                        "type" : "array",
                                        "items": { "type": "integer" },
                                        "description":"0: Walkable ; 1: Not Walkable"
                                    }
                                }
                            }
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "schema": {
                            "$ref": "#/definitions/MapsPostResponse"
                        }
                    },
                    "400": {
                        "description": "Error occured, more details in body.error",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    }
                }
            },
            "patch": {
                "tags": [ "Maps" ],
                "summary": "Update a map to the world",
                "parameters": [
                    {
                        "in": "body",
                        "name": "body",
                        "description": "To access a tile, tileMap[y][x][layer]",
                        "required": true,
                        "schema": {
                            "type": "object",
                            "properties": {
                                "id": { "type": "string" },
                                "name": { "type": "string" },
                                "tileMap": {
                                    "type": "array",
                                    "items": {
                                        "type": "array",
                                        "items": {
                                            "type": "array",
                                            "minItems": "5",
                                            "maxItems": "5",
                                            "items": { "type": "integer" }
                                        }
                                    }
                                },
                                "hitboxMap": {
                                    "type" : "array",
                                    "items": {
                                        "type" : "array",
                                        "items": { "type": "integer" },
                                        "description":"0: Walkable ; 1: Not Walkable"
                                    }
                                }
                            }
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "schema": {
                            "$ref": "#/definitions/MapsPatchResponse"
                        }
                    },
                    "400": {
                        "description": "Error occured, more details in body.error",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    }
                }
            }
        },
        "/maps/{id}": {
            "get": {
                "tags": [ "Maps" ],
                "summary": "Returns the map",
                "parameters": [
                    {
                        "in": "path",
                        "name": "id",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "schema": {
                            "$ref": "#/definitions/MapsGetResponse"
                        }
                    },
                    "400": {
                        "description": "Error occured, more details in body.error",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    }
                }
            },
            "delete": {
                "tags": [ "Maps" ],
                "summary": "Delete permanently the map of given id from prefeb",
                "parameters": [
                    {
                        "in": "path",
                        "name": "id",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "schema": {
                            "$ref": "#/definitions/MapsDeleteResponse"
                        }
                    },
                    "400": {
                        "description": "Error occured, more details in body.error",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    }
                }
            }
        },
        "/defaultMap": {
            "get": {
                "tags": [ "DefaultMap" ],
                "summary": "Returns the spawn for all players",
                "responses": {
                    "200": {
                        "schema": {
                            "$ref": "#/definitions/DefaultMapGetResponse"
                        }
                    },
                    "400": {
                        "description": "Error occured, more details in body.error",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    }
                }
            },
            "put": {
                "tags": [ "DefaultMap" ],
                "summary": "Set the first map the player will spawn in, as well as the tile coordinates",
                "parameters": [
                    {
                        "in": "body",
                        "name": "body",
                        "required": true,
                        "schema": {
                            "type": "object",
                            "properties": {
                                "mapId": { "type": "string" },
                                "spawn": {
                                    "type": "object",
                                    "properties": {
                                        "x": { "type": "integer" },
                                        "y": { "type": "integer" }
                                    }
                                }
                            }
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "schema": {
                            "$ref": "#/definitions/DefaultMapPutResponse"
                        }
                    },
                    "400": {
                        "description": "Error occured, more details in body.error",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    }
                }
            }
        },
        "/games/{id}": {
            "post":{
                "tags": [ "Games" ],
                "summary": "Create a new game",
                "parameters": [
                    {
                        "in": "path",
                        "name": "id",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "schema": {
                            "$ref": "#/definitions/GamePostResponse"
                        }
                    },
                    "400": {
                        "description": "Error occured, more details in body.error",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    }
                }
            },
            "get": {
                "tags": [ "Games" ],
                "summary": "Return the game's player state",
                "parameters": [
                    {
                        "in": "path",
                        "name": "id",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "schema": {
                            "$ref": "#/definitions/GameGetResponse"
                        }
                    },
                    "400": {
                        "description": "Error occured, more details in body.error",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    }
                }
            },
            "delete": {
                "tags": [ "Games" ],
                "summary": "Delete all occurrences of the maps associate with the game's id",
                "parameters": [
                    {
                        "in": "path",
                        "name": "id",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "schema": {
                            "$ref": "#/definitions/GameDeleteResponse"
                        }
                    },
                    "400": {
                        "description": "Error occured, more details in body.error",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    }
                }
            }
        },
        "/games/{id}/users": {
            "post": {
                "tags": [ "Games" ],
                "summary": "Advance player position on the current map of the given game's id",
                "parameters": [
                    {
                        "in": "path",
                        "name": "id",
                        "required": true
                    },
                    {
                        "in": "body",
                        "name": "body",
                        "required": true,
                        "schema": {
                            "type": "object",
                            "properties": {
                                "direction": {
                                    "type": "enum",
                                    "enum": [
                                        "North",
                                        "South",
                                        "West",
                                        "East"
                                    ]
                                },
                                "distance": { "type": "integer" }
                            }
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "schema": {
                            "$ref": "#/definitions/GameMovementResponse"
                        }
                    },
                    "201": {
                        "schema": {
                            "$ref": "#/definitions/GameMovementInvalidResponse"
                        }
                    },
                    "400": {
                        "description": "Error occured, more details in body.error",
                        "schema": {
                            "$ref": "#/definitions/ResponseError"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "VersionResponse": createApiResponseDefinition("#/definitions/VersionResponseData"),
        "VersionResponseData": {
            "type": "string",
            "description": "{major}.{minor}.{patch} or {major}.{minor}.{patch}-{label}"
        },
        "AllMapsGetResponse": createApiResponseDefinition("#/definitions/AllMapsGetResponseData"),
        "AllMapsGetResponseData": {
            "type": "array",
            "items": {
                "type": "object",
                "properties": {
                    "id": { "type": "string" },
                    "name": { "type": "string" },
                    "tileMap": {
                        "type": "array",
                        "items": {
                            "type": "array",
                            "items": {
                                "type": "array",
                                "minItems": "5",
                                "maxItems": "5",
                                "items": { "type": "integer" }
                            }
                        }
                    },
                    "hitboxMap": {
                        "type" : "array",
                        "items": {
                            "type" : "array",
                            "items": { "type": "integer" },
                            "description":"0: Walkable ; 1: Not Walkable"
                        }
                    }
                }
            }
        },
        "MapsPostResponse": createApiResponseDefinition("#/definitions/MapsPostResponseData"),
        "MapsPostResponseData": {
            "type": "int",
            "description": "Id of map added"
        },
        "MapsPatchResponse": createApiResponseDefinition("#/definitions/MapsPatchResponseData"),
        "MapsPatchResponseData": {
            "type": "string",
            "description": "Map successfully updated"
        },
        "MapsGetResponse": createApiResponseDefinition("#/definitions/MapsGetResponseData"),
        "MapsGetResponseData": {
            "type": "object",
            "properties": {
                "id": { "type": "string" },
                "name": { "type": "string" },
                "tileMap": {
                    "type": "array",
                    "items": {
                        "type": "array",
                        "items": {
                            "type": "array",
                            "minItems": "5",
                            "maxItems": "5",
                            "items": { "type": "integer" }
                        }
                    }
                },
                "hitboxMap": {
                    "type" : "array",
                    "items": {
                        "type" : "array",
                        "items": { "type": "integer" },
                        "description":"0: Walkable ; 1: Not Walkable"
                    }
                }
            }
        },
        "MapsDeleteResponse": createApiResponseDefinition("#/definitions/MapsDeleteResponseData"),
        "MapsDeleteResponseData": {
            "type": "string",
            "description": "Map successfully deleted"
        },
        "DefaultMapGetResponse": createApiResponseDefinition("#/definitions/DefaultMapGetResponseData"),
        "DefaultMapGetResponseData": {
            "type": "object",
            "properties": {
                "mapId": { "type": "string" },
                "x": { "type": "integer" },
                "y": { "type": "integer" }
            }
        },
        "DefaultMapPutResponse": createApiResponseDefinition("#/definitions/DefaultMapPutResponseData"),
        "DefaultMapPutResponseData": {
            "type": "string",
            "description": "Spawn successfully set"
        },
        "GameGetResponse": createApiResponseDefinition("#/definitions/GameGetResponseData"),
        "GameGetResponseData": {
            "type": "object",
            "properties": {
                "mapId": { "type": "string" },
                "x": { "type": "integer" },
                "y": { "type": "integer" }
            }
        },
        "GamePostResponse" : createApiResponseDefinition("#/definitions/GamePostResponseData"),
        "GamePostResponseData": {
            "type": "string",
            "description": "Game successfully instantiated"
        },
        "GameDeleteResponse": createApiResponseDefinition("#/definitions/GameDeleteResponseData"),
        "GameDeleteResponseData": {
            "type": "string",
            "description": "Game successfully deleted"
        },
        "GameMovementResponse": createApiResponseDefinition("#/definitions/GameMovementResponseData"),
        "GameMovementResponseData": {
            "type": "object",
            "properties": {
                "playerPosition": { 
                    "type": "object",
                    "properties": {
                        "x": { "type": "integer" },
                        "y": { "type": "integer" }
                    }
                }
            }
        },
        "GameMovementInvalidResponse": createApiResponseDefinition("#/definitions/GameMovementInvalidResponseData"),
        "GameMovementInvalidResponseData": {
            "type": "object",
            "properties": {
                "badMove": { 
                    "type": "boolean"
                }
            },
            "description": "Move Invalid"
        },
        "ResponseError": createApiResponseDefinition("#/definitions/ResponseErrorData"),
        "ResponseErrorData": {
            "type": "string",
            "description": "Error message"
        }
    }
};

export default swaggerConfig;
