/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included
 */
import { Router } from "express";
import Map from "../../../models/map/index";
import Spawn from "../../../models/spawn/index";
import { ApiErrorResponse, ApiResponse } from "@rpga/common";
import { v4 as uuidv4 } from "uuid";
import { validate as uuidValidate } from "uuid";
 
const router: Router = Router();
 
 
router.get("/", async(req, res) => {  
    if(req.query.start !== undefined && req.query.size !== undefined) {
        const start = isNaN(Number.parseInt(String(req.query.start))) ? 0 : Number.parseInt(String(req.query.start));
        const size = isNaN(Number.parseInt(String(req.query.size))) ? Number.MAX_SAFE_INTEGER : Number.parseInt(String(req.query.size));

        
        if(start < 0 || size < 0 || !Number.isFinite(start) || !Number.isFinite(size)) {
            res.status(400).send(new ApiErrorResponse("Invalid query"));
            return;
        }

        if(req.query.search !== undefined){
            const maps = await Map.find( { name: new RegExp(String(req.query.search), "i") })
                .skip(start)
                .limit(size);
            res.send(new ApiResponse(maps));
            return;
        } else {
            const maps = await Map.find()
                .skip(start)
                .limit(size);
            res.send(new ApiResponse(maps));
            return;
        }    
        
    } else {
        const maps = await Map.find();
        res.send(new ApiResponse(maps));
    }
});
 
router.get("/:id", async(req, res) => {
    const mapId = req.params.id;
    if(!uuidValidate(mapId)) {
        res.status(400).send(new ApiErrorResponse("Invalid uuid"));
        return;
    }

    if (await Map.exists({ id: mapId })) {
        res.send(new ApiResponse(await Map.findOne({ id: mapId })));
    } else {
        res.status(400).send(new ApiErrorResponse("No map found for specified id"));
    }
});
 
router.post("/", async(req, res) => {
    // Generate uuid for each new maps
    const customId = uuidv4();
    const newData = new Map({
        id: customId,
        name: req.body.name,
        tileMap: req.body.tileMap,
        hitboxMap: req.body.hitboxMap
    });
    await newData.save();
    res.send(new ApiResponse(customId));
});

router.patch("/", async(req, res) => {
    const mapId = req.body.id;
    if(!uuidValidate(mapId)) {
        res.status(400).send(new ApiErrorResponse("Invalid uuid"));
        return;
    }

    if (await Map.exists({ id: mapId })) {
        await Map.updateOne({ id: mapId }, {
            name: req.body.name,
            tileMap: req.body.tileMap,
            hitboxMap: req.body.hitboxMap 
        });
        res.send(new ApiResponse("Map successfully updated"));
    } else {
        res.status(400).send(new ApiErrorResponse("No map found for specified id"));
    }
});
 
router.delete("/:id", async(req, res) => {
    const mapId = req.params.id;
    if(!uuidValidate(mapId)) {
        res.status(400).send(new ApiErrorResponse("Invalid uuid"));
        return;
    }

    if (await Spawn.exists({ mapId: mapId })){
        res.status(400).send(new ApiErrorResponse("You cannot delete the default map"));
        return;
    }

    if (await Map.exists({ id: mapId })) {
        await Map.findOneAndDelete({ id: mapId });
        res.send(new ApiResponse("Map successfully deleted"));
    } else {
        res.status(400).send(new ApiErrorResponse("No map found for specified id"));
    }
});
 
export default router;
 