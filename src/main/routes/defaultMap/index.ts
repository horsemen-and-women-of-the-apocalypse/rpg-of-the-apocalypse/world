/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included
 */
import { Router } from "express";
import Spawn from "../../../models/spawn/index";
import { ApiErrorResponse, ApiResponse } from "@rpga/common";
import { validate as uuidValidate } from "uuid";

const router: Router = Router();

router.get("/", async(req, res) => {
    const result = await Spawn.findOne();
    if(result === null) {
        res.status(400).send(new ApiErrorResponse("Spawn is not set"));
    } else {
        res.send(new ApiResponse({ mapId: result.mapId, x: result.x, y: result.y }));
    }
});

router.put("/", async(req, res) => {
    if(!uuidValidate(req.body.mapId)) {
        res.status(400).send(new ApiErrorResponse("Invalid uuid"));
        return;
    }
    
    //TODO: Does not work
    if ( isNaN(parseInt(req.body.spawn.x)) || req.body.spawn.x < 0 || isNaN(parseInt(req.body.spawn.y)) || req.body.spawn.x < 0 ) {
        res.status(400).send(new ApiErrorResponse("Invalid arguments for spawn coordinates"));
        return;
    }

    const isSet = await Spawn.exists({});
    if (isSet) {
        await Spawn.updateOne({}, {
            mapId: req.body.mapId,
            x: req.body.spawn.x,
            y: req.body.spawn.y
        });          
    }
    else {
        const newData = new Spawn({
            mapId: req.body.mapId,
            x: req.body.spawn.x,
            y: req.body.spawn.y
        });
        await newData.save();
    }
    res.send(new ApiResponse("Spawn successfully set"));
});

export default router;
