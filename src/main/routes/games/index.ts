/**
 * @licence
 * Copyright 2022-2022 - RPG of the Apocalypse
 * Licensed under MIT or any later version
 * Refer to the LICENSE file included
 */
import { Router } from "express";
import { ApiErrorResponse, ApiResponse } from "@rpga/common";
import Spawn from "../../../models/spawn";
import GameInstance from "../../../models/gameInstance";
import Map from "../../../models/map";
import mongoose from "mongoose";

const locks: { [name: string]: boolean } = {};

const router: Router = Router();

router.post("/:id", async(req, res) => {
    if(!mongoose.isValidObjectId(req.params.id)) {
        res.status(400).send(new ApiErrorResponse("Given id is not a mongo's ObjectId"));
        return;
    } 
    const gameId = req.params.id;
    
    if(await GameInstance.exists({ id: gameId })) {
        res.status(400).send(new ApiErrorResponse("Game with same id already exists"));
        return;
    }

    if (!await Spawn.exists({})) {
        res.status(400).send(new ApiErrorResponse("No spawn has been defined yet. Unable to instantiate a game"));
    } else {
        const spawn = await Spawn.findOne();
        const game = new GameInstance({
            id: `${gameId}`,
            currentMapId: spawn.mapId,
            x: spawn.x,
            y: spawn.y,
        });
        await game.save();
        res.send(new ApiResponse("Game successfully instantiated"));
    }
});

router.delete("/:id", async(req, res) => {
    if(!mongoose.isValidObjectId(req.params.id)) {
        res.status(400).send(new ApiErrorResponse("Given id is not a mongo's ObjectId"));
        return;
    } 
    const gameId = req.params.id;
    
    if(!await GameInstance.exists({ id: gameId })) {
        res.status(400).send(new ApiErrorResponse("No game found for specified id"));
        return;
    }
    
    if (await GameInstance.exists({ id: gameId })) {
        await GameInstance.findOneAndDelete({ id: gameId });
        res.send(new ApiResponse("Game successfully deleted"));
    } else {
        res.status(400).send(new ApiErrorResponse("No game found for specified id"));
    }
});

router.get("/:id", async(req, res) => {
    if(!mongoose.isValidObjectId(req.params.id)) {
        res.status(400).send(new ApiErrorResponse("Given id is not a mongo's ObjectId"));
        return;
    } 
    const gameId = req.params.id;

    if(await GameInstance.exists({ id: gameId })) {
        const gameInstance = await GameInstance.findOne({ id: gameId });
        res.send(new ApiResponse({ mapId: gameInstance.currentMapId, x: gameInstance.x, y: gameInstance.y }));
    } else {
        res.status(400).send(new ApiErrorResponse("No game found for specified id"));
    }
});

router.post("/:id/users", async(req, res) => {
    if(!mongoose.isValidObjectId(req.params.id)) {
        res.status(400).send(new ApiErrorResponse("Given id is not a mongo's ObjectId"));
        return;
    } 
    const gameId = req.params.id;
    
    if(!await GameInstance.exists({ id: gameId })) {
        res.status(400).send(new ApiErrorResponse("No game found for specified id"));
        return;
    }

    const distance = parseInt(req.body.distance);
    const gameInstance = await GameInstance.findOne({ id: gameId });
    const map = await Map.findOne({ id: gameInstance.currentMapId });
    const mapDimensions = {
        x: map.hitboxMap[0].length,
        y: map.hitboxMap.length
    };
    
    if(locks[req.params.id]) {
        res.status(400).send(new ApiErrorResponse("Move ignored: earlier move did not finish for this game"));
        return;
    }
    locks[req.params.id] = true;

    const initialPos = {
        x: gameInstance.x,
        y: gameInstance.y
    };

    function isMoveValid(directionFunction: (arg0: { x: number; y: number; }) => { x: number; y: number; }) {
        let pos = initialPos;
            
        for(let i = 0; i < distance; i ++) {
            pos = directionFunction(pos);
            // check if we don't get out of the boundaries or in a wall
            if(pos.x < 0 || pos.x >= mapDimensions.x || pos.y < 0 || pos.y >= mapDimensions.y || map.hitboxMap[pos.y][pos.x] == 1) {
                return false;
            }
        }
            
        return true;
    }

    if (distance >= 0) {
        // tileMap[y][x][layer]
        switch (req.body.direction) {
            case "North":
                if(isMoveValid(arg0 => ({ x: arg0.x, y: arg0.y - 1 }))) {
                    await GameInstance.findOneAndReplace({ id: gameId }, {
                        id: gameInstance.id,
                        currentMapId: gameInstance.currentMapId,
                        x: gameInstance.x,
                        y: gameInstance.y - distance
                    });
                    res.status(200).send(new ApiResponse({ "playerPosition": { "x":gameInstance.x, "y": gameInstance.y - distance } }));
                } else {
                    //res.status(400).send(new ApiErrorResponse("Invalid Move"));
                    res.status(201).send(new ApiResponse({ "badMove": true }));
                }
                break;
            case "South":
                if(isMoveValid(arg0 => ({ x: arg0.x, y: arg0.y + 1 }))) {
                    await GameInstance.findOneAndReplace({ id: gameId }, {
                        id: gameInstance.id,
                        currentMapId: gameInstance.currentMapId,
                        x: gameInstance.x,
                        y: gameInstance.y + distance
                    });
                    res.status(200).send(new ApiResponse({ "playerPosition": { "x":gameInstance.x, "y": gameInstance.y + distance } }));
                } else {
                    //res.status(400).send(new ApiErrorResponse("Invalid Move"));
                    res.status(201).send(new ApiResponse({ "badMove": true }));
                }
                break;
            case "West":
                if(isMoveValid(arg0 => ({ x: arg0.x - 1, y: arg0.y }))) {
                    await GameInstance.findOneAndReplace({ id: gameId }, {
                        id: gameInstance.id,
                        currentMapId: gameInstance.currentMapId,
                        x: gameInstance.x - distance,
                        y: gameInstance.y
                    });
                    res.status(200).send(new ApiResponse({ "playerPosition": { "x":gameInstance.x - distance, "y": gameInstance.y } }));
                } else {
                    //res.status(400).send(new ApiErrorResponse("Invalid Move"));
                    res.status(201).send(new ApiResponse({ "badMove": true }));
                }
                break;
            case "East":
                if(isMoveValid(arg0 => ({ x: arg0.x + 1, y: arg0.y }))) {
                    await GameInstance.findOneAndReplace({ id: gameId }, {
                        id: gameInstance.id,
                        currentMapId: gameInstance.currentMapId,
                        x: gameInstance.x + distance,
                        y: gameInstance.y
                    });
                    res.status(200).send(new ApiResponse({ "playerPosition": { "x":gameInstance.x + distance, "y": gameInstance.y } }));
                } else {
                    //res.status(400).send(new ApiErrorResponse("Invalid Move"));
                    res.status(201).send(new ApiResponse({ "badMove": true }));
                }
                break;
            default:
                res.status(400).send(new ApiErrorResponse("Unknown direction token"));
        }
    } else {
        res.status(400).send(new ApiErrorResponse("Invalid distance"));
    }
    locks[req.params.id] = false;
});

export default router;
