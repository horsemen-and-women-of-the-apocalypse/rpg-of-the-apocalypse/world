import chai, { expect } from "chai";
import chaiHttp from "chai-http";
import server from "../../main/index";
import mongoUnit from "mongo-unit";
import { describe } from "mocha";
import Spawn from "../../models/spawn";
import mongoose from "mongoose";
import Map from "../../models/map";
import GameInstance from "../../models/gameInstance";
import { validate as uuidValidate } from "uuid";

chai.use(chaiHttp);

describe("Routes", () => {

    async function createMaps() {
        const newData1 = new Map({
            id: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
            name: "0",
            tileMap: [ [ [ 0 ] ] ],
            hitboxMap: [ [ 0 ] ]
        });
        const newData2 = new Map({
            id: "c1bd0205-648f-464d-95b9-a8d4ac0d7445",
            name: "1",
            tileMap: [ [ [ 1 ] ] ],
            hitboxMap: [ [ 1 ] ]
        });
        await newData1.save();
        await newData2.save();
    }

    beforeEach(async() => {
        await mongoose.connection.dropDatabase();
    });
    afterEach(() => mongoUnit.drop());

    describe("Map", () => {
        describe("the (POST/PATCH)/ route", () => {
            it("should allow adding a map", async() => {
                await chai.request(server).post("/maps", )
                    .send({
                        name: "i forgor",
                        tileMap: [ [ [ 0 ] ] ],
                        hitboxMap: [ [ 0 ] ]
                    })
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(uuidValidate(res.body.data)).to.be.true;
                        expect(res.body.error).to.equal(null);
                    });
            });
            
            it("should allow updating a map", async() => {
                await createMaps();
                await chai.request(server).patch("/maps", )
                    .send({
                        id: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                        name: "New Name",
                        tileMap: [ [ [ 0 ] ] ],
                        hitboxMap: [ [ 0 ] ]
                    })
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.equal("Map successfully updated");
                    });
            });
        });

        describe("the GET/ route", () => {
            it("should return an empty array when no map are set", async() => {
                await chai.request(server).get("/maps")
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data.length).to.equal(0);
                        expect(res.body.error).to.equal(null);
                    });
            });

            it("should return the proper map list", async() => {
                await createMaps();

                await chai.request(server).get("/maps")
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data.length).to.equal(2);
                        expect(res.body.data[0]).to.include({
                            id: "2cf73c54-d78e-47a7-bd3f-3599070c60b3"
                        });
                        expect(res.body.data[1]).to.include({
                            id: "c1bd0205-648f-464d-95b9-a8d4ac0d7445"
                        });
                    });
            });

            it("should return the proper map list (query incomplete)", async() => {
                await createMaps();

                await chai.request(server).get("/maps?search=0&start=0")
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data.length).to.equal(2);
                        expect(res.body.data[0]).to.include({
                            id: "2cf73c54-d78e-47a7-bd3f-3599070c60b3"
                        });
                        expect(res.body.data[1]).to.include({
                            id: "c1bd0205-648f-464d-95b9-a8d4ac0d7445"
                        });
                    });
            });

            it("should return the proper map list (query invalid)", async() => {
                await createMaps();

                const newData = new Map({
                    id: "6bbd016e-a4b6-41fa-aa6d-67db9573ff27",
                    name: "1",
                    tileMap: [ [ [ 1 ] ] ],
                    hitboxMap: [ [ 1 ] ]
                });

                await newData.save();

                await chai.request(server).get("/maps?search=0&start=-1&size=1")
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.data).to.equal("KO");
                        expect(res.body.error).to.equal("Invalid query");
                    });
            });

            it("should return the selected map list (get first results)", async() => {
                await createMaps();

                const newData = new Map({
                    id: "6bbd016e-a4b6-41fa-aa6d-67db9573ff27",
                    name: "1",
                    tileMap: [ [ [ 1 ] ] ],
                    hitboxMap: [ [ 1 ] ]
                });

                await newData.save();

                await chai.request(server).get("/maps?search=1&start=0&size=1")
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data.length).to.equal(1);
                        expect(res.body.data[0]).to.include({
                            id: "c1bd0205-648f-464d-95b9-a8d4ac0d7445"
                        });
                    });
            });

            it("should return the selected map list (get all result except first one (query oversize))", async() => {
                await createMaps();

                const newData = new Map({
                    id: "6bbd016e-a4b6-41fa-aa6d-67db9573ff27",
                    name: "1",
                    tileMap: [ [ [ 1 ] ] ],
                    hitboxMap: [ [ 1 ] ]
                });

                const newData2 = new Map({
                    id: "343d59c0-bc4c-4ce0-b6f5-dc1f81e15957",
                    name: "1",
                    tileMap: [ [ [ 1 ] ] ],
                    hitboxMap: [ [ 1 ] ]
                });

                await newData.save();
                await newData2.save();

                await chai.request(server).get("/maps?search=1&start=1&size=12")
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data.length).to.equal(2);
                        expect(res.body.data[0]).to.include({
                            id: "6bbd016e-a4b6-41fa-aa6d-67db9573ff27"
                        });
                        expect(res.body.data[1]).to.include({
                            id: "343d59c0-bc4c-4ce0-b6f5-dc1f81e15957"
                        });
                    });
            });
        });

        describe("the GET/:id route", () => {
            it("should return an error when trying to access non existing map (not an uuid)", async() => {
                await createMaps();

                await chai.request(server).get("/maps/3")
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.data).to.equal("KO");
                        expect(res.body.error).to.equal("Invalid uuid");
                    });
            });

            it("should return an error when trying to access a non existing map (not in DB)", async() => {
                await createMaps();

                await chai.request(server).get("/maps/7d9651ee-4149-4dec-acec-546fc3b69500")
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.data).to.equal("KO");
                        expect(res.body.error).to.equal("No map found for specified id");
                    });
            });

            it("should return the proper map list", async() => {
                await createMaps();

                await chai.request(server).get("/maps/2cf73c54-d78e-47a7-bd3f-3599070c60b3")
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.include({
                            id: "2cf73c54-d78e-47a7-bd3f-3599070c60b3"
                        });
                    });
            });
        });

        describe("the DELETE/:id route", () => {
            it("should return an error when trying to delete a non existing map (not an uuid)", async() => {
                await createMaps();

                await chai.request(server).delete("/maps/3")
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.data).to.equal("KO");
                        expect(res.body.error).to.equal("Invalid uuid");
                    });
            });

            it("should return an error when trying to delete a non existing map (not in DB)", async() => {
                await createMaps();

                await chai.request(server).delete("/maps/7d9651ee-4149-4dec-acec-546fc3b69500")
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.data).to.equal("KO");
                        expect(res.body.error).to.equal("No map found for specified id");
                    });
            });

            it("should return an error when trying to delete the default map", async() => {
                await createMaps();

                await chai.request(server).put("/defaultMap" )
                    .send({
                        mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                        spawn: {
                            x: 0,
                            y: 0
                        }
                    })
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.equal("Spawn successfully set");
                        expect(res.body.error).to.equal(null);
                    });

                await chai.request(server).delete("/maps/2cf73c54-d78e-47a7-bd3f-3599070c60b3")
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.data).to.equal("KO");
                        expect(res.body.error).to.equal("You cannot delete the default map");
                    });
            });

            it("should return the proper map list", async() => {
                await createMaps();

                await chai.request(server).delete("/maps/2cf73c54-d78e-47a7-bd3f-3599070c60b3")
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.equal("Map successfully deleted");
                    });
                
                const maps = await Map.find();
                expect(maps.length).to.equal(1);
                expect(maps[0]).to.include({ id: "c1bd0205-648f-464d-95b9-a8d4ac0d7445" });
            });
        });
    });

    describe("DefaultMap", () => {
        it("should let you ADD the spawn", async() => {
            await chai.request(server).put("/defaultMap" )
                .send({
                    mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    spawn: {
                        x: 0,
                        y: 0
                    }
                })
                .then(res => {
                    expect(res.status).to.equal(200);
                    expect(res.body.data).to.equal("Spawn successfully set");
                    expect(res.body.error).to.equal(null);
                });

            await Spawn.find({ mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3" }).then(res => {
                expect(res.length).to.equal(1);
                expect(res[0]).to.include({ mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3", x: 0, y: 0 } );
            });
        });

        it("should not let you GET an undifined spawn", async() => {
            await chai.request(server).get("/defaultMap" )
                .then(res => {
                    expect(res.status).to.equal(400);
                    expect(res.body.data).to.equal("KO");
                    expect(res.body.error).to.equal("Spawn is not set");
                });
        });

        it("should let you GET the spawn", async() => {
            await chai.request(server).put("/defaultMap" )
                .send({
                    mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    spawn: {
                        x: 0,
                        y: 0
                    }
                })
                .then(res => {
                    expect(res.status).to.equal(200);
                    expect(res.body.data).to.equal("Spawn successfully set");
                    expect(res.body.error).to.equal(null);
                });

            await chai.request(server).get("/defaultMap" )
                .then(res => {
                    expect(res.status).to.equal(200);
                    expect(res.body.data).to.include({ mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3", x: 0, y: 0 });
                    expect(res.body.error).to.equal(null);
                });
        });

        it("should not let you EDIT the spawn (not an uuid)", async() => {
            await createMaps();

            await chai.request(server).put("/defaultMap" )
                .send({
                    mapId: "3",
                    spawn: {
                        x: 0,
                        y: 0
                    }
                })
                .then(res => {
                    expect(res.status).to.equal(400);
                    expect(res.body.data).to.equal("KO");
                    expect(res.body.error).to.equal("Invalid uuid");
                });
        });

        it("should let you EDIT the spawn", async() => {
            // Set the spawn
            await chai.request(server).put("/defaultMap" )
                .send({
                    mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    spawn: {
                        x: 0,
                        y: 0
                    }
                })
                .then(res => {
                    expect(res.status).to.equal(200);
                    expect(res.body.data).to.equal("Spawn successfully set");
                    expect(res.body.error).to.equal(null);
                });

            // Change the spawn
            await chai.request(server).put("/defaultMap" )
                .send({
                    mapId: "c1bd0205-648f-464d-95b9-a8d4ac0d7445",
                    spawn: {
                        x: 1,
                        y: 1
                    }
                })
                .then(res => {
                    expect(res.status).to.equal(200);
                    expect(res.body.data).to.equal("Spawn successfully set");
                    expect(res.body.error).to.equal(null);
                });

            // Look up in mongo. There shall be 1 spawn, edited
            await Spawn.find().then(res => {
                expect(res.length).to.equal(1);
                expect(res[0]).to.include({ mapId: "c1bd0205-648f-464d-95b9-a8d4ac0d7445", x: 1, y: 1 } );
            });
        });
    });

    describe("Game", () => {
        describe("the POST/:id route", () => {
            it("Should not POST a map without an uuid", async() => {
                await createMaps();
                const spawn = new Spawn({
                    mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await spawn.save();

                await chai.request(server).post("/games/1")
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.error).to.equal("Given id is not a mongo's ObjectId");
                    });

                const instance = await GameInstance.find({ id: "62627d6bac0e4887be4253d6" });
                expect(instance.length).to.equal(0);
            });

            it("Should automatically spawn the player", async() => {
                await createMaps();
                const spawn = new Spawn({
                    mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await spawn.save();

                await chai.request(server).post("/games/62627d6bac0e4887be4253d6")
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.equal("Game successfully instantiated");
                    });

                const instance = await GameInstance.find({ id: "62627d6bac0e4887be4253d6" });
                expect(instance.length).to.equal(1);
            });

            it("Should send error if spawn hasn't been set yet", async() => {
                await createMaps();

                await chai.request(server).post("/games/62627d6bac0e4887be4253d6")
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.error).to.equal("No spawn has been defined yet. Unable to instantiate a game");
                    });

                const instance = await GameInstance.find();
                expect(instance.length).to.equal(0);
            });

            it("Should send error if instance with same id already exists", async() => {
                await createMaps();
                const spawn = new Spawn({
                    mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await spawn.save();
                const instance = new GameInstance({
                    id: "62627d6bac0e4887be4253d6",
                    currentMapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await instance.save();

                await chai.request(server).post("/games/62627d6bac0e4887be4253d6")
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.error).to.equal("Game with same id already exists");
                    });
            });
        });

        describe("the GET/:id route", () => {
            it("Should not GET a map without an uuid", async() => {
                await createMaps();
                const spawn = new Spawn({
                    mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await spawn.save();
                const instance = new GameInstance({
                    id: "62627d6bac0e4887be4253d6",
                    currentMapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await instance.save();

                await chai.request(server).get("/games/1")
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.error).to.equal("Given id is not a mongo's ObjectId");
                    });
            });

            it("Should respond accurate data", async() => {
                await createMaps();
                const spawn = new Spawn({
                    mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await spawn.save();
                const instance = new GameInstance({
                    id: "62627d6bac0e4887be4253d6",
                    currentMapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await instance.save();

                await chai.request(server).get("/games/62627d6bac0e4887be4253d6")
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.include({
                            mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                            x: 0,
                            y: 0
                        });
                    });
            });

            it("Should respond error when requested wrong id", async() => {
                await createMaps();
                const spawn = new Spawn({
                    mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await spawn.save();
                const instance = new GameInstance({
                    id: "62627d6bac0e4887be4253d6",
                    currentMapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await instance.save();

                await chai.request(server).get("/games/6262cb7d475ab71c491df1e5")
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.error).to.equal("No game found for specified id");
                    });
            });
        });

        describe("the DELETE/:id route", () => {

            it("Should not DELETE a map without an uuid", async() => {
                await createMaps();
                const spawn = new Spawn({
                    mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await spawn.save();
                const instance = new GameInstance({
                    id: "62627d6bac0e4887be4253d6",
                    currentMapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await instance.save();

                await chai.request(server).delete("/games/1")
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.data).to.equal("KO");
                        expect(res.body.error).to.equal("Given id is not a mongo's ObjectId");
                    });

                const games = await GameInstance.find();
                expect(games.length).to.equal(1);
            });

            it("Should delete the right game instance", async() => {
                await createMaps();
                const spawn = new Spawn({
                    mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await spawn.save();
                const instance = new GameInstance({
                    id: "62627d6bac0e4887be4253d6",
                    currentMapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await instance.save();

                await chai.request(server).delete("/games/62627d6bac0e4887be4253d6")
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.equal("Game successfully deleted");
                    });

                const games = await GameInstance.find();
                expect(games.length).to.equal(0);
            });

            it("Should respond error when requested wrong id", async() => {
                await createMaps();
                const spawn = new Spawn({
                    mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await spawn.save();
                const instance = new GameInstance({
                    id: "62627d6bac0e4887be4253d6",
                    currentMapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await instance.save();

                await chai.request(server).delete("/games/6262cb7d475ab71c491df1e5")
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.error).to.equal("No game found for specified id");
                    });
            });
        });

        describe("the POST/:id/user route", () => {
            it("should res an error when id does not exist", async() => {
                await chai.request(server).post("/games/6262cdf3475ab71c491df1ef/users")
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.error).to.equal("No game found for specified id");
                    });
            });

            it("should res an error if distance or direction is invalid", async() => {
                await createMaps();
                const spawn = new Spawn({
                    mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await spawn.save();
                const instance = new GameInstance({
                    id: "62627d6bac0e4887be4253d6",
                    currentMapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await instance.save();

                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        distance: -1,
                        direction: "South"
                    })
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.error).to.equal("Invalid distance");
                    });

                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        distance: 1,
                        direction: "SOOOOOOOOO"
                    })
                    .then(res => {
                        expect(res.status).to.equal(400);
                        expect(res.body.error).to.equal("Unknown direction token");
                    });
            });

            it("should prevent going outside the map borders", async() => {
                await createMaps();
                const spawn = new Spawn({
                    mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await spawn.save();
                const instance = new GameInstance({
                    id: "62627d6bac0e4887be4253d6",
                    currentMapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 0,
                    y: 0
                });
                await instance.save();

                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "North",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(201);
                        expect(res.body.data).to.be.deep.equal({ badMove : true });
                    });
                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "South",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(201);
                        expect(res.body.data).to.be.deep.equal({ badMove : true });
                    });
                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "West",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(201);
                        expect(res.body.data).to.be.deep.equal({ badMove : true });
                    });
                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "East",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(201);
                        expect(res.body.data).to.be.deep.equal({ badMove : true });
                    });
            });
            
            it("should block the player that tries to move into an object", async() => {
                const newData1 =
                    new Map({
                        id: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                        name: "Some Name",
                        tileMap: [ [ [ 0 ] ] ],
                        hitboxMap: 
                            [
                                [ 0, 1, 0 ],
                                [ 1, 0, 1 ],
                                [ 0, 1, 0 ]
                            ]
                    });
                await newData1.save();

                const spawn = new Spawn({
                    mapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 1,
                    y: 1
                });
                await spawn.save();

                const instance = new GameInstance({
                    id: "62627d6bac0e4887be4253d6",
                    currentMapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 1,
                    y: 1
                });
                await instance.save();

                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "North",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(201);
                        expect(res.body.data).to.be.deep.equal({ badMove : true });
                    });
                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "South",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(201);
                        expect(res.body.data).to.be.deep.equal({ badMove : true });
                    });
                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "West",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(201);
                        expect(res.body.data).to.be.deep.equal({ badMove : true });
                    });
                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "East",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(201);
                        expect(res.body.data).to.be.deep.equal({ badMove : true });
                    });
            });

            it("should let the player move around", async() => {
                const newData1 =
                    new Map({
                        id: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                        name: "Just a name",
                        tileMap: [ [ [ 0 ] ] ],
                        hitboxMap:
                            [
                                [ 0, 0 ],
                                [ 0, 0 ]
                            ]
                    });
                await newData1.save();

                const spawn = new Spawn({
                    mapId: 0,
                    x: 1,
                    y: 1
                });
                await spawn.save();

                const instance = new GameInstance({
                    id: "62627d6bac0e4887be4253d6",
                    currentMapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 1,
                    y: 1
                });
                await instance.save();

                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "North",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.be.deep.equal({ playerPosition : { x:1, y:0 } });
                    });
                
                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "South",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.be.deep.equal({ playerPosition : { x:1, y:1 } });
                    });
                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "West",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.be.deep.equal({ playerPosition : { x:0, y:1 } });
                    });
                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "East",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.be.deep.equal({ playerPosition : { x:1, y:1 } });
                    });
            });

            it("should not let the player move with distance > 1 on a hitbox tile (clipping)", async() => {
                const newData1 =
                    new Map({
                        id: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                        name: "Just a name",
                        tileMap: [ [ [ 0 ] ] ],
                        hitboxMap:
                            [
                                [ 1, 1, 1, 1 ],
                                [ 1, 0, 0, 1 ],
                                [ 1, 0, 0, 1 ],
                                [ 1, 1, 1, 1 ]
                            ]
                    });
                await newData1.save();

                const spawn = new Spawn({
                    mapId: 0,
                    x: 1,
                    y: 1
                });
                await spawn.save();

                const instance = new GameInstance({
                    id: "62627d6bac0e4887be4253d6",
                    currentMapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 1,
                    y: 1
                });
                await instance.save();

                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "South",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.be.deep.equal({ playerPosition : { x:1, y:2 } });
                    });

                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "North",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.be.deep.equal({ playerPosition : { x:1, y:1 } });
                    });

                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "South",
                        distance: 2
                    })
                    .then(res => {
                        expect(res.status).to.equal(201);
                        expect(res.body.data).to.be.deep.equal({ badMove : true });
                    });
            });

            it("should not let the player move with distance > 1 past a hitbox tile (out of bounds)", async() => {
                const newData1 =
                    new Map({
                        id: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                        name: "Lambda",
                        tileMap: [ [ [ 0 ] ] ],
                        hitboxMap:
                            [
                                [ 1, 1, 1, 1 ],
                                [ 1, 0, 0, 1 ],
                                [ 1, 0, 0, 1 ],
                                [ 1, 1, 1, 1 ]
                            ]
                    });
                await newData1.save();

                const spawn = new Spawn({
                    mapId: 0,
                    x: 1,
                    y: 1
                });
                await spawn.save();

                const instance = new GameInstance({
                    id: "62627d6bac0e4887be4253d6",
                    currentMapId: "2cf73c54-d78e-47a7-bd3f-3599070c60b3",
                    x: 1,
                    y: 1
                });
                await instance.save();

                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "South",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.be.deep.equal({ playerPosition : { x:1, y:2 } });
                    });

                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "North",
                        distance: 1
                    })
                    .then(res => {
                        expect(res.status).to.equal(200);
                        expect(res.body.data).to.be.deep.equal({ playerPosition : { x:1, y:1 } });
                    });

                await chai.request(server).post("/games/62627d6bac0e4887be4253d6/users")
                    .send({
                        direction: "South",
                        distance: 3
                    })
                    .then(res => {
                        expect(res.status).to.equal(201);
                        expect(res.body.data).to.be.deep.equal({ badMove : true });
                    });
            });
        });
    });
});

after(() => {
    server.close();
    return mongoUnit.stop();
});
