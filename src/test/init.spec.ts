import mongoUnit from "mongo-unit";
import LOGGER from "../main/logger";

mongoUnit.start().then(() => {
    LOGGER.info("Mongo is started: ", mongoUnit.getUrl());
    run();
});

after(() => {
    return mongoUnit.stop();
});
