import mongoose from "mongoose";

const gameInstance = new mongoose.Schema({
    id: {
        type: String,
        required: true
    },
    currentMapId: {
        type: String,
        required: true
    },
    x: {
        type: Number,
        required: true
    },
    y: {
        type: Number,
        required: true
    }
});

const GameInstance = mongoose.model("GameInstance", gameInstance);

export default GameInstance;
