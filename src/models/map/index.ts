import mongoose from "mongoose";

const map = new mongoose.Schema({
    id: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    tileMap:
    [
        [
            [ {
                type: Number
            } ]
        ]
    ],
    hitboxMap: 
    [
        [ {
            type: Number
        } ]
    ]
});

const Map = mongoose.model("Map", map);

export default Map;
