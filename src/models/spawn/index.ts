import mongoose from "mongoose";

const spawn = new mongoose.Schema({
    mapId: {
        type: String,
        required: true
    },
    x: {
        type: Number,
        required: true
    },
    y: {
        type: Number,
        required: true
    }
});

const Spawn = mongoose.model("Spawn", spawn);

export default Spawn;